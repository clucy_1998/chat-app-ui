import React from 'react';
import './App.css';


import theme from './themes/theme';
import { ThemeProvider } from '@material-ui/core';
import ChatLayout from "./components/Chat/ChatLayout/ChatLayout";
import NavBar from './components/AppBar/AppBar'
import SignIn from './components/Auth/SignIn/SignIn';
import SignUp from './components/Auth/SignUp/SignUp';
import SettingsLayout from './components/Settings/SettingsLayout';
import ProfileLayout from './components/Profile/ProfileLayout/ProfileLayout';
import OtherProfileLayout from './components/OtherProfile/ProfileLayout/OtherProfileLayout'
import { Redirect } from 'react-router'

//Imports for Routing
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";


function App() {

  let isLoggedIn = localStorage.getItem("isLoggedIn")

  if (isLoggedIn === "true") {

    return (
      <div className="App">
        <ThemeProvider theme={theme}>
          <Router>
            <NavBar />
            <Switch>
              <Route path="/signin" component={SignIn} />
              <Route path="/signup" component={SignUp} />
              <Route path="/chat" component={ChatLayout} />
              <Route path="/settings" component={SettingsLayout} />
              <Route path="/profile" component={ProfileLayout} />
              <Route path="/user" component={OtherProfileLayout} />
            </Switch>
          </Router>
        </ThemeProvider>

      </div>


    );
  } else {
    return (
      <div className="App">
        <ThemeProvider theme={theme}>
          <Router>
            <NavBar />
            <Switch>
              <Route path="/signin" component={SignIn} />
              <Route path="/signup" component={SignUp} />
              <Route exact path="/signin">
                {isLoggedIn ? <Redirect to="/chat" /> : <ChatLayout />}
              </Route>
            </Switch>
          </Router>
        </ThemeProvider>

      </div>
    )
  }


}


export default App
