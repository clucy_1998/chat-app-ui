import { createMuiTheme } from '@material-ui/core/styles';
// import {red, green, purple, blue, yellow, indigo} from '@material-ui/core/colors';



const theme = createMuiTheme({
  palette: {
    // primary: indigo,
    // secondary: blue,
  },
  status: {
    // danger: 'orange',
  },
  props: {
    // Name of the component ⚛️
    MuiButtonBase: {
      // The properties to apply
      disableRipple: false, // No more ripple, on the whole application 💣!
    },
  },
  // root: {
  //   "& $notchedOutline": {
  //     borderWidth: 0
  //   },
  //   "&:hover $notchedOutline": {
  //     borderWidth: 0
  //   },
  //   "&$focused $notchedOutline": {
  //     borderWidth: 0
  //   }
  // }

});

export default theme;