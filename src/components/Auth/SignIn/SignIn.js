import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { login } from '../../../redux'
import { Redirect } from 'react-router'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


//Imports Material UI Components
import { Avatar, Button, Checkbox, Grid, Box, Typography, FormControlLabel, Container, TextField, CssBaseline } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

//Imports for Routing
import { Link } from 'react-router-dom';

//Imports for styling/Always last
import { useStyles } from "./styles/signIn";


function LoginForm(props) {
  const classes = useStyles();
  const [formData, setFormData] = useState(
    {
      email: '',
      password: ''
    }
  )
  const { email, password } = formData
  let isLoggedIn = localStorage.getItem("isLoggedIn")



  const changeHandler = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  }

  const submitHandler = e => {

    e.preventDefault()
    console.log(formData)
    props.login(formData)

    setFormData({
      ...formData,
      email: '',
      password: ''
    })

    //window.location.reload(true);
    //props.setLocal()


  }

  useEffect(() => {
    return () =>
      <Redirect to='/Chat' />

  }, [isLoggedIn])

  if (isLoggedIn === "true") {

    return (
      <Redirect to='/chat' />
    )
  } else {
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
            </Typography>
          <form className={classes.form} noValidate onSubmit={submitHandler}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              value={email}
              onChange={changeHandler}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={password}
              onChange={changeHandler}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
              </Button>
            <Grid container>
              <Grid item xs>
                <Link to="#" variant="body2">
                  Forgot password?
                  </Link>
              </Grid>
              <Grid item>
                <Link to="/signup" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={8}>
          {/* <Copyright /> */}
        </Box>
      </Container>
    );
  }

}



const mapDispatchToProps = (dispatch) => {
  return {
    login: (formData) => dispatch(login(formData))
  }
}

export default connect(null, mapDispatchToProps)(LoginForm)
