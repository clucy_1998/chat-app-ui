import React, { useState } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import { Redirect } from 'react-router'
//Imports Material UI Components
import { Avatar, Button, TextField, Grid, Box, Typography } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Container from '@material-ui/core/Container';

//Imports for Routing
import { Link } from 'react-router-dom';

//Imports for styling/Always last
import { useStyles } from "./styles/signUp";
//Notification config
toast.configure()

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}



export default function SignUp() {
  const classes = useStyles();

  const [formData, setFormData] = useState(
    {
      firstName: '',
      lastName: '',
      email: '',
      username: '',
      password: ''

    }
  )

  const changeHandler = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })

  }

  const submitHandler = e => {
    e.preventDefault()
    axios.post('http://localhost:5000/api/user/register', formData)
      .then(response => {
        console.log(response)
        setFormData({
          ...formData,
          firstName: '',
          lastName: '',
          email: '',
          username: '',
          password: ''
        })
        window.location.reload(true);

      })
      .catch(error => {
        console.log(error.response)
        toast.error(error.response.data, {
          autoClose: 5000,
          hideProgressBar: true,
          className: classes.toastError
        })
      })
  }

  const { firstName, lastName, email, username, password } = { formData }
  let isLoggedIn = localStorage.getItem("isLoggedIn")
  if (isLoggedIn === "true") {

    return (
      <Redirect to='/chat' />
    )
  } else {

    return (
      <React.Fragment>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign up
        </Typography>
            <form className={classes.form} noValidate onSubmit={submitHandler}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="fname"
                    name="firstName"
                    variant="outlined"
                    required
                    fullWidth
                    id="firstName"
                    label="First Name"
                    autoFocus
                    value={firstName}
                    onChange={changeHandler}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="lastName"
                    label="Last Name"
                    name="lastName"
                    autoComplete="lname"
                    value={lastName}
                    onChange={changeHandler}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    value={email}
                    onChange={changeHandler}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="username"
                    label="Username"
                    name="username"
                    autoComplete="uname"
                    value={username}
                    onChange={changeHandler}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    value={password}
                    onChange={changeHandler}
                  />
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}

              >
                Sign Up
          </Button>
              <Grid container justify="flex-end">
                <Grid item>
                  <Link to="/signin" variant="body2">
                    Already have an account? Sign in
              </Link>
                </Grid>
              </Grid>
            </form>
          </div>
          <Box mt={5}>
            <Copyright />
          </Box>
        </Container>
      </React.Fragment>

    );
  }
}