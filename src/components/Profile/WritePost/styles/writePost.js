import { makeStyles } from '@material-ui/core'


const useStyles = makeStyles((theme) => ({
    container: {
        //backgroundColor: 'orange',
        height: '100%',
        paddingTop: '10px'

    },
    textContainer: {
        border: '1px solid black',
        height: '50px',

        //padding: '15px'
    },
    writePost: {
        margin: 'auto',
        height: '100%',
        width: '90%',
        //  backgroundColor: 'yellow',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    textField: {
        backgroundColor: 'rgba(0, 0, 0, .07)',
        borderRadius: '15px',
        padding: '5px 10px',
        width: '100%',
        height: "100%"

        // '& textarea': {
        //     // backgroundColor: 'red',
        //     margin: '-10px',
        //     border: 'none',
        //     '& input': {
        //         align: 'center'
        //     }

        // }
    },
    input: {
        display: 'none'
    },
    buttonsContainer: {
        width: '100%',
        display: 'flex',
        // alignItems: 'flex-start',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: '10px'
        // backgroundColor: 'red',
        // textAlign: 'left'


    },
    cameraButtonIcon: {
        // alignSelf: 'left'
        //backgroundColor: 'red'

    },
    postButton: {
        // alignSelf: 'right'
    },
    miniatureFile: {
        height: "45px",

        '& img': {
            height: '100%',
            width: 'auto',
            borderRadius: '10px'
        },

    },
    displayNone: {
        display: 'none'
    }




}

));

export { useStyles }