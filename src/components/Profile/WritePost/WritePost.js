import React, { useState, useEffect } from 'react'

import { connect } from 'react-redux'
import { LoadingPosts } from '../../../redux';
//aterial UI imports
import { Box, Divider, IconButton, Button, InputBase } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import theme from '../../../themes/theme'
import { useStyles } from './styles/writePost.js'
import { postAPost } from './axiosCalls'

function WritePost(props) {

    const classes = useStyles(theme);

    //STATES////////////////////////////


    let [file, setFile] = useState({});
    let [miniatureFile, setMiniatureFile] = useState({});
    let [min, setMin] = useState();
    const [writePostText, setWritePostText] = useState('');
    //File Select Handler
    const fileSelectedHandler = (event) => {
        console.log(event.target.files[0])
        if (event.target.files[0]) {
            setFile(event.target.files[0])
            setMiniatureFile(URL.createObjectURL(event.target.files[0]))
        }


    }
    //Upload Handle
    const uploadHandle = () => {
        //Clear write post and min
        setMiniatureFile({});
        setWritePostText('')
        //Send it
        const formData = new FormData();

        formData.append("content", writePostText)
        formData.append("file", file);
        postAPost(formData, props.LoadingPosts);

        setFile({});
    }
    const handleWritePost = (e) => {
        setWritePostText(e.target.value)
    }

    useEffect(() => {
        if (Object.entries(miniatureFile).length === 0) {
            setMin(<div></div>);
        }
        else {
            setMin(
                <img alt="miniature img" src={miniatureFile} />
            )
        }
    }, [file, miniatureFile])

    return (
        <div className={classes.container}>

            <Box className={classes.writePost}>

                <InputBase className={classes.textField}
                    inputProps={{ 'aria-label': 'naked' }}
                    rows={1}
                    rowsMax={2}
                    multiline
                    variant="outlined"
                    value={writePostText}
                    onChange={handleWritePost}
                />
                <div className={classes.buttonsContainer}>
                    <label htmlFor="icon-button-file" >
                        <Box display="flex" justifyContent="flex-start" alignItems="center" >
                            <IconButton className={classes.cameraButtonIcon} color="primary" aria-label="upload picture" component="span" >
                                <AddAPhotoIcon />
                                <input accept="image/*" className={classes.input} id="icon-button-file" type="file" name="file" onChange={fileSelectedHandler} />

                            </IconButton>

                            <div className={classes.miniatureFile}>
                                {min}
                            </div>
                        </Box>


                    </label>
                    <Button size="small" color="primary" variant="contained" className={classes.postButton} onClick={uploadHandle}>Post</Button >
                </div>

            </Box>

            <Divider className={classes.divider} />
        </div>
    )
}



const mapDispatchToProps = dispatch => {
    return {
        LoadingPosts: () => dispatch(LoadingPosts()),

    }
}

export default connect(null, mapDispatchToProps)(WritePost)
