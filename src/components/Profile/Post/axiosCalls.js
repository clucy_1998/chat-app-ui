import axios from 'axios'
//Get User Images for Posts
export const getUserImgPosts = (id, setImgSource) => {
    console.log("Id ul postarii")
    console.log(id);
    axios.get('http://localhost:5000/api/user/profile/imgposts', {
        params: {
            id: id
        }, responseType: 'arraybuffer',
        withCredentials: true,


    })
        .then((response) => {
            const base64 = btoa(
                new Uint8Array(response.data).reduce(
                    (data, byte) => data + String.fromCharCode(byte),
                    '',
                ),
            );
            console.log("BAAAAAAASE")

            setImgSource(base64);

        });
}

//Delete Posts here

export const deletePost = (post, LoadingPosts) => {

    axios.delete('http://localhost:5000/api/user/profile/posts', {
        params: {
            post: post,
        },

        withCredentials: true
    })
        .then(response => {
            LoadingPosts();
        })
}

//Get Likes

//Get User Images for Posts
export const getLikes = (postId, setNumOfLikes, setUsersWhoLiked) => {
    axios.get('http://localhost:5000/api/user/profile/likes', {
        params: {
            postId: postId
        },
        withCredentials: true,
    })
        .then((response) => {
            setNumOfLikes(response.data.numOfLikes);
            setUsersWhoLiked(response.data.userList)
        });
}

