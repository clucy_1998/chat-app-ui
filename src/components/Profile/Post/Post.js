import React, { useState, useEffect } from 'react'

import { connect } from 'react-redux'
import { LoadingPosts } from '../../../redux';

//Import Notification 
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
//Imports for Styling
import { useStyles } from './styles/post'

//Imports Material UI
import { Box, IconButton, Button, Zoom, List, ListItem, Popper } from '@material-ui/core'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import Snackbar from '@material-ui/core/Snackbar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import CloseIcon from '@material-ui/icons/Close';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';


//Axios Calls
import { getUserImgPosts, deletePost, getLikes } from './axiosCalls'
function Post(props) {
    //states
    //likes
    const [numOfLikes, setNumOfLikes] = useState(0);
    const [usersWhoLiked, setUsersWhoLiked] = useState([]);
    //images
    let [imgSource, setImgSource] = useState(null);
    let [imgOfPost, setImgOfPost] = useState(null);
    const [anchorEl, setAnchorEl] = useState(null);
    const [anchorElPopper, setAnchorElPopper] = useState(null);
    //Handles



    const handleMenuClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleMenuClose = () => {
        setAnchorEl(null);
    };


    //Delete Post
    const deletePostHandler = () => {
        handleClose();
        handleMenuClose();
        deletePost(props.postId, props.LoadingPosts)
        // getConv(setContacts, setConv);
        // setLoading(!loading);
        toast.success("Succesfully deleted post", {
            position: toast.POSITION.BOTTOM_LEFT,
            hideProgressBar: true,
            transition: Zoom,
            autoClose: 2000,
            className: classes.toastSuccess
        })
    }
    //Popper

    const handleClickPopper = (event) => {
        setAnchorElPopper(anchorElPopper ? null : event.currentTarget);
    };

    //SnackBar
    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };
    //effects
    useEffect(() => {
        console.log('useeffectccccccccccccccccc')
        //GetImage
        getUserImgPosts(props.postId, setImgSource);
        getLikes(props.postId, setNumOfLikes, setUsersWhoLiked);
    }, [props.postId])

    const classes = useStyles();
    useEffect(() => {
        if (!imgSource) {
            setImgOfPost(<div></div>);
        }
        else {
            setImgOfPost(

                <img alt="post" src={`data:image/jpg;base64,${imgSource}`}></img>
            )
        }
    }, [imgSource])

    const openPopper = Boolean(anchorElPopper);
    const id = openPopper ? 'simple-popper' : undefined;


    return (
        <div className={classes.postContainer}>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleMenuClose}
            >
                <MenuItem className={classes.menu} onClick={handleClick}>Delete</MenuItem>

            </Menu>
            <Snackbar
                className={classes.snackbar}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={open}
                autoHideDuration={6000}
                onClick={handleClose}
                message="Delete this post?"
                action={
                    < React.Fragment >
                        <Button color="secondary" size="small" onClick={deletePostHandler} >
                            DELETE
            </Button>
                        <IconButton size="small" aria-label="close" color="inherit">
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment >
                }
            />
            <Box className={classes.content}>

                <Box display="flex" flexDirection="column" className={classes.postTextContainer}>
                    <Box display="flex" justifyContent="space-between" alignItems="center" className={classes.moreButtonContainer} >
                        <div className={classes.date}> {props.postedAt}

                        </div>
                        <IconButton size="small" className={classes.moreButton} aria-controls="simple-menu" aria-haspopup="true" onClick={handleMenuClick}>
                            <MoreHorizIcon className={classes.moreIcon} />
                        </IconButton>


                    </Box>

                    <div className={classes.text}>
                        {props.postContent}
                    </div>

                </Box>

                <div className={classes.imgContainer}>
                    {imgOfPost}
                </div>

                <Box display="flex" justifyContent="start" className={classes.postTextContainer}>
                    <Box display="flex" alignItems="center" className={classes.text}>
                        <IconButton aria-describedby={id} type="button" onClick={handleClickPopper}>
                            <ThumbUpIcon />
                        </IconButton>
                        {numOfLikes}
                    </Box>
                    <Popper id={id} open={openPopper} anchorEl={anchorElPopper}>

                        <div className={classes.userListContainer}>
                            <List>
                                {
                                    usersWhoLiked.map((user, index) => {
                                        return (
                                            <ListItem key={index}>
                                                {user}
                                            </ListItem>
                                        )
                                    })
                                }
                            </List>
                        </div>


                    </Popper>

                </Box>

            </Box>


        </div>
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        loadingPosts: state.post.loadingPosts,
        postId: ownProps.post._id,
        postContent: ownProps.post.content,
        postedAt: ownProps.post.postedAt
    }
}

const mapDispatchToProps = dispatch => {
    return {
        LoadingPosts: () => dispatch(LoadingPosts()),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)
