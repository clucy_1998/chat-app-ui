import { makeStyles } from '@material-ui/core'


const useStyles = makeStyles((theme) => ({
    postContainer: {
        backgroundColor: 'white',
        margin: '5px 0px',
        //  border: '1px solid rgba(0, 0, 0, .07)',
        paddingBottom: ' 5px',
        // borderTopLeftRadius: "10px",
        // borderTopRightRadius: '10px'

    },
    content: {
        maxWidth: '100%',
        background: 'white',
        padding: '0px',
        // borderTopLeftRadius: "10px",
        // borderTopRightRadius: '10px'
    },
    imgContainer: {
        marginTop: '5px',

        padding: '15px',
        '& img': {
            height: '100%',
            width: '100%',
            borderRadius: '10px'
        },
    },
    postTextContainer: {
        width: '100%',
        overflowWrap: 'break-all',
        wordWrap: 'break-all',
        hyphens: 'auto',
        //  backgroundColor: 'blue',
    },
    text: {
        paddingLeft: '15px',
        height: '100%',
        // backgroundColor: 'pink',
        textAlign: 'left',
        overflowWrap: 'break-word',
        wordWrap: 'break-word',
        hyphens: 'auto',
    },
    moreButtonContainer: {
        textAlign: 'right',
        backgroundColor: '#3f51b5',
        marginBottom: '15px',
        // borderTopLeftRadius: "10px",
        // borderTopRightRadius: '10px',
        padding: '0px 10px'
    },
    moreButton: {
        paddingRight: '5px',
        width: '30px',
        color: 'white'
    },
    moreIcon: {

    },
    date: {
        color: 'white'
    },
    toastSuccess: {
        backgroundColor: theme.palette.success.main,
        textAlign: 'center'
    },
    userListContainer: {
        border: '1px solid',
        padding: theme.spacing(1),
        backgroundColor: theme.palette.background.paper,
        maxHeight: '150px',
        overflowY: 'auto',

    },

}));

export { useStyles }