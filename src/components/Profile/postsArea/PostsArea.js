import React, { useState, useEffect } from 'react'

import { connect } from 'react-redux'
import { NotLoadingPosts } from '../../../redux';
//Import Components
import Post from '../Post/Post'
//Axios Calls
import { getUserPosts } from './axiosCalls'
//Imports for Styling
import { useStyles } from './styles/postsArea'
//aterial UI imports
import { Box } from '@material-ui/core';




function PostsArea(props) {

    //states
    let loadingPostsState = props.loadingPosts;
    // let [postLoading, setPostLoading] = useState(false);
    let [posts, setPosts] = useState([]);


    useEffect(() => {
        getUserPosts(setPosts, props.NotLoadingPosts);
        console.log("usefect for actualii puting the posts")

    }, [loadingPostsState, props.NotLoadingPosts])
    const classes = useStyles();
    return (
        <Box display="flex" flexDirection="column-reverse" className={classes.postsContainer}>
            {
                posts.map((post, index) => {
                    return <Post key={index} post={post} />
                })
            }
        </Box>
    )
}


const mapStateToProps = state => {
    return {
        loadingPosts: state.post.loadingPosts,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        NotLoadingPosts: () => dispatch(NotLoadingPosts())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(PostsArea)

