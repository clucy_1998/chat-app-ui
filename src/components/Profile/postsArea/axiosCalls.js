import axios from 'axios'

//Get User Posts
export const getUserPosts = (setPosts, NotLoadingPosts) => {
    axios.get('http://localhost:5000/api/user/profile/posts', { withCredentials: true })
        .then((response) => {
            console.log('Aicii get user posts');
            console.log(response.data)
            setPosts(response.data);
            NotLoadingPosts();
        });
}
