import React from 'react'

//Import components
import WritePost from '../WritePost/WritePost'
import PostsArea from '../postsArea/PostsArea'
//Imports Material UI
import { Grid, Box } from '@material-ui/core'


//Imports for Styling
import { useStyles } from './styles/contentArea'

function ContentArea() {
    const classes = useStyles();
    return (

        <Box display="flex" flexDirection="column">
            <div>
                <WritePost />
            </div>
            <div>
                <PostsArea />
            </div>
        </Box>
    )
}

export default ContentArea
