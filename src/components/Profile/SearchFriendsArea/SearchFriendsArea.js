import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { setCurrentUser } from '../../../redux';

//Imports for Routing
import {
    Link
} from "react-router-dom";
//Imports for Styling
import { useStyles } from './styles/searchFriendsArea'

//Imports Material UI
import { Box, InputBase, IconButton, List, ListItem } from '@material-ui/core'
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';

//Import Components
import SingleContact from '../../Chat/SingleContact/SingleContact'

//Axios Callas
import { getAppUsers } from './axiosCalls'

function SearchFriendsArea(props) {
    const classes = useStyles();
    //States
    let [list, setList] = useState();
    const [appUsers, setAppUsers] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");
    const user = {
        id: `${props.userId}`,
        username: `${props.userUsername}`,
        firstName: `${props.userfirstName}`,
        lastName: `${props.userlastName}`
    }
    //Handles
    const handleSearchChange = e => {
        if (e.target.value != null) {
            setSearchTerm(e.target.value);
            const searchResults = appUsers.filter(user =>
                user.firstName.toLowerCase().includes((searchTerm).toLowerCase()) ||
                user.lastName.toLowerCase().includes((searchTerm).toLowerCase()) ||
                user.username.toLowerCase().includes((searchTerm).toLowerCase())
            )
                ;

            //(role => role._id.find(group => user.groups.includes(group._id)));
            //  setContacts(searchResults);
            setAppUsers(searchResults)
        }
        if (e.target.value.trim() === '') {
            setAppUsers([]);
            //   getConv(setContacts, setConv);

        }

    };

    //Clear search
    const clearInput = () => {
        setSearchTerm('')
        setAppUsers([])
    }
    //Use Effect
    useEffect(() => {

        getAppUsers(setAppUsers);
        console.log(appUsers)
        //getConv(setContacts, setConv);

        console.log('use effect get  users')

        setList(
            appUsers.map((user) => {
                return (

                    <Link to="/user" key={user._id} className={classes.link} style={{ textDecoration: 'none' }}>
                        <ListItem button className={classes.listItemStyle} onClick={() => { props.setCurrentUser(user) }} >


                            <SingleContact to="/user" contact={user} />

                        </ListItem >
                    </Link>

                )

            })
        )
    }, [searchTerm])


    return (
        <Box display="flex" alignItems="center" flexDirection="column" className={classes.container}>
            <Box display="flex" alignItems="center" className={classes.searchBarContainer}>
                <SearchIcon />
                <InputBase
                    placeholder="Search…"
                    inputProps={{ 'aria-label': 'search' }}
                    value={searchTerm}
                    onChange={handleSearchChange}
                    variant="outlined"
                />
                <IconButton size="small" onClick={clearInput} >
                    <ClearIcon className={classes.clearButton} />
                </IconButton>
            </Box>
            <div className={classes.usersListContainer}>
                <List>
                    {list}
                </List>


            </div>

        </Box>


    )
}
const mapStateToProps = state => {
    return {
        currentUser: state.otherUser.user,
        username: state.otherUser.userUsername,
        userId: state.otherUser.userId,
        userfirstName: state.otherUser.userfirstName,
        userlastName: state.otherUser.userlastName,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setCurrentUser: (currentUser) => dispatch(setCurrentUser(currentUser)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchFriendsArea)

