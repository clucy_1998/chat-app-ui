import { makeStyles } from '@material-ui/core'


const useStyles = makeStyles(() => ({
    layout: {
        //height: 'calc(100vh - 64px)',
        //backgroundColor: 'yellow',
        marginTop: '64px'
    },
    one: {
        //backgroundColor: 'red',
        // borderRight: '1px solid  rgba(0, 0, 0, .2) ',
        height: '100%'
    },
    two: {
        //  backgroundColor: 'blue',
        // height: '100%'pentru
        height: '100%',
        borderRight: '1px solid  rgba(0, 0, 0, .2) ',
        borderLeft: '1px solid  rgba(0, 0, 0, .2) ',
        display: 'block',
        overflowWrap: 'break-all',

    },
    tree: {
        //backgroundColor: 'green',
        height: '100%',
        display: 'flex',
        alignItems: 'start',
        justifyContent: 'center',
        //borderLeft: '1px solid  rgba(0, 0, 0, .2) ',
        paddingTop: '10px'

    }

}));

export { useStyles }