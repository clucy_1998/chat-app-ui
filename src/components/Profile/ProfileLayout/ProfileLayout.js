import React from 'react'

//Imports Material UI
import { Grid, Box } from '@material-ui/core'

//Imports for Styling
import { useStyles } from './styles/profileLayout'

//Import components
import UserArea from '../UserArea/UserArea'
import ContentArea from '../ContentArea/ContentArea';
import SearchFriendsArea from '../SearchFriendsArea/SearchFriendsArea'

function ProfileLayout() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container className={classes.layout} >
                <Grid item md={3} sm={3} xs={12} className={classes.one}>
                    <UserArea />
                </Grid>
                <Grid item md={6} sm={6} xs={12} className={classes.two}>
                    <ContentArea />
                </Grid>
                <Grid item md={3} sm={3} xs={12} className={classes.tree}>
                    <Box display="flex" alignItems="center">
                        <SearchFriendsArea />
                    </Box>

                </Grid>
            </Grid>
        </div>
    )
}

export default ProfileLayout

