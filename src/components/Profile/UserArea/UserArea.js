import React, { useState, useEffect } from 'react'

//Imports Material UI Components
import { Avatar, Box } from '@material-ui/core/';
//import { flexbox } from '@material-ui/system';

//Imports for Styling
import { useStyles } from './styles/userArea';
import theme from '../../../themes/theme';

import { getUserData, getUserPhoto } from './axiosCalls';
function UserArea() {
    //STATES////////////////////////////
    let [file, setFile] = useState(null);

    let [imgSource, setImgSource] = useState(null);
    let [user, setUser] = useState({});

    const { firstName, lastName, email, username } = user;

    useEffect(() => {
        console.log('useeffectUserArea')

        //AXIOS
        //Get User Data
        getUserData(setUser);

        //Get User Photo
        getUserPhoto(setImgSource);

    }, [imgSource])
    const classes = useStyles(theme);
    return (
        <Box display="flex" alignItems="center" flexDirection="column" justifyContent="space-around" className={classes.userAreaContainer}>
            <Box className={classes.imageContainer}>
                <Avatar className={classes.avatar} sizes='1' alt="You profile picture" src={`data:image/jpg;base64,${imgSource}`}></Avatar>
            </Box>
            <Box>
                {username}
            </Box>

        </Box>

    )
}

export default UserArea
