import axios from 'axios'

//Get User Data

export const getUserData = (setUser) => {
    axios.get('http://localhost:5000/api/user/userDetails', { withCredentials: true })
        .then(response => {
            console.log("details");
            setUser(response.data);
        })
}

//Get User Photo
export const getUserPhoto = (setImgSource) => {
    axios.get('http://localhost:5000/api/user/userDetails/photo', { responseType: 'arraybuffer', withCredentials: true })
        .then((response) => {
            console.log('Aicii get profile pic');
            const base64 = btoa(
                new Uint8Array(response.data).reduce(
                    (data, byte) => data + String.fromCharCode(byte),
                    '',
                ),
            );

            setImgSource(base64);
        });
}
