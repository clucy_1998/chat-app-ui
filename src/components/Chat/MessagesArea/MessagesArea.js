import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux'
import Message from '../Message/Message'
//Socket
import io from "socket.io-client";
//Import axiosCalls
import { getOldMessages } from './axiosCalls'

import { Typography, Box, Avatar } from '@material-ui/core'
import { useStyles } from './styles/messagesArea'
const fistBump = require('../../../public/img/fist-bump.svg');
const addSvg = require('../../../public/img/add.svg');


//Socket endpoint
const hostURL = "http://localhost:5000";
const socket = io(hostURL, {
        transports: ["websocket", "polling"],
});

socket.on('new message', (data) => {
        console.log(data)
})


function MessagesArea(props) {
        const boxRef = useRef();
        const classes = useStyles();
        //States
        const [messages, setMessages] = useState([]);
        const [newMessages, setNewMessages] = useState([]);
        let [userId, setUserId] = useState(null);

        const contact = {
                id: `${props.contactId}`,
                name: `${props.contactName}`,
                status: `${props.contactStatus}`
        }

        //Get old messages

        useEffect(() => {
                getOldMessages(contact.id, setMessages, setUserId);
                console.log('Mesaje normale')
                console.log(messages);
                setNewMessages([]);
                console.log("NEW MESSAG")
                console.log(newMessages)


        }, [contact.id]);
        useEffect(() => {
                socket.on('new message', (data) => {
                        console.log("AIIIIIIIIIICIIIII")
                        console.log(data[0][0].content);
                        // setNewMessages(data[0]);
                        setNewMessages(newMessages => [...newMessages, data[0][0]]);
                        console.log("NEW MSG")
                        console.log(newMessages)
                        console.log("NEW MESSAG")
                        console.log(newMessages);
                })

        }, [])



        if (contact.id) {
                try {
                        return (

                                <Box ref={boxRef} id="scroll" display='flex' flexDirection="column-reverse" className={classes.chatMessageList}>
                                        <Box display='flex' flexDirection="column">
                                                {
                                                        newMessages.map((msg, index) => {

                                                                return <Message key={index} message={msg} userId={userId} />
                                                                // return <li key={index}>{msg.content}</li>
                                                        })
                                                }
                                        </Box>

                                        <Box display='flex' flexDirection="column-reverse">
                                                {messages.slice(0).reverse().map((message) => {
                                                        return (

                                                                <Message key={message._id} message={message} userId={userId} />

                                                        )


                                                })}
                                        </Box>

                                </Box>

                        )
                } catch{
                        return (

                                <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" className={classes.pickAFriendContainer}>
                                        <Avatar variant="square" alt="Pick a friend" src={String(addSvg)} className={classes.pickAFriendImg} />
                                        <Typography color="textSecondary" variant="subtitle1">
                                                Add this contact to start sending messages.
                                        </Typography>
                                </Box>
                        )
                }

        } else {
                return (

                        <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" className={classes.pickAFriendContainer}>
                                <Box className={classes.pickAFriendImg} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                                        <Avatar variant="circle" alt="Pick a friend" src={String(fistBump)} className={classes.avatar} />
                                        <Typography color="textSecondary" variant="subtitle1">
                                                Pick a conversation
                                </Typography>
                                </Box>


                        </Box>

                )

        }


}


const mapStateToProps = (state) => {
        return {
                contactName: state.activeContact.contactName,
                contactId: state.activeContact.contactId,
                contactStatus: state.activeContact.contactStatus
        }
}
export default connect(mapStateToProps)(MessagesArea)

