import axios from 'axios'

//Get Old Messages

export const getOldMessages = (id, setMessages, setUserId) => {
    axios.get('http://localhost:5000/api/chat/conversations/messages', {
        params: {
            contactId: id
        }, withCredentials: true
    })
        .then(response => {

            console.log(response.data[0]);
            console.log(response.data[1]);
            setUserId(response.data[1].user);
            setMessages(response.data[0]);


        })

}
