import { makeStyles } from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({
  chatMessageList: {
    // backgroundColor: 'yellow',
    // display: 'flex',
    // flexDirection: 'column',
    // justifyContent: 'flex-end',
    marginTop: 'auto',
    height: '100%',
    overflowY: 'scroll',

    scrollbarWidth: '5px',
  },
  pickAFriendContainer: {
    //backgroundColor: 'red',
    height: '100%',
    width: '100%'
  },
  pickAFriendImg: {
    // backgroundColor: 'pink',
    height: '100%',
    width: '50%'
  },
  avatar: {
    height: "auto",
    width: '75%',

  }

}));

export { useStyles }