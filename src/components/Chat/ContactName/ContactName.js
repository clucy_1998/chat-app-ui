import React from 'react'
import { connect } from 'react-redux'
import SingleContact from '../SingleContact/SingleContact'

import { Paper, Typography } from '@material-ui/core'

//Imports for styling
import { useStyles } from './styles/contactName';
function UserName(props) {

  const classes = useStyles();
  const contact = {
    id: `${props.contactId}`,
    username: `${props.contactUsername}`,
    firstName: `${props.contactfirstName}`,
    lastName: `${props.contactlastName}`,
    status: `${props.contactStatus}`
  }
  if (contact.id) {
    return (

      <React.Fragment>
        <Paper square={true} elevation={0} className={classes.paperStyle}>
          <Typography component={'span'} className={classes.contactNameStyle}>
            <SingleContact contact={contact} />
          </Typography>
        </Paper>
      </React.Fragment>
    );
  } else {
    return (
      <div>

      </div>
    )
  }

}
const mapStateToProps = state => {
  return {
    contactUsername: state.activeContact.contactUsername,
    contactId: state.activeContact.contactId,
    contactStatus: state.activeContact.contactStatus,
    contactfirstName: state.activeContact.contactfirstName,
    contactlastName: state.activeContact.contactlastName,
  }
}
export default connect(mapStateToProps)(UserName)
