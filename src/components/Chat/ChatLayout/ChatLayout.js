import React from 'react';

//Imports Material UI Components
import { Grid, Typography } from '@material-ui/core/';


//Imports for Components
import ContactList from '../ContactList/ContactList';
import ContactName from '../ContactName/ContactName';
import MessagesArea from '../MessagesArea/MessagesArea';
import WriteMessage from '../WriteMessage/WriteMessage';

//Imports for styling
import { useStyles } from './styles/chatLayout';

// function ListItemLink(props) {
//   return <ListItem button component="a" {...props} />;
// }

export default function ChatLayout() {
  const classes = useStyles();


  return (
    <div >
      <Grid container className={classes.mainContainer}> {/* Doesn't do much. Just sets the space between the grids.*/}
        <Grid item md={3} sm={3} xs={12} className={classes.contactsAreaStyle}>
          <div className={classes.titleStyle}>
            <Typography color="primary">
              Contacts
          </Typography>
          </div>
          <ContactList />
        </Grid>
        <Grid item md={9} sm={9} xs={12} className={classes.chatAreaStyle}>
          <Grid item md={12} className={classes.currentUserStyle} >
            <ContactName />
          </Grid>

          <Grid item md={12} className={classes.messagesAreaStyle} >
            <MessagesArea />
          </Grid>

          <Grid item md={12} className={classes.writeMessageStyle} >
            <WriteMessage />
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
