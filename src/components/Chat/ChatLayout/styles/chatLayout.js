import { makeStyles } from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({

  mainContainer: {
    //backgroundColor:'white',
    marginTop: '64px',
    height: 'calc(100vh - 64px)',

  },
  contactsAreaStyle: {
    //backgroundColor: 'pink',
    height: '100%',
    overflowY: 'auto',
    borderRight: '1px solid  rgba(0, 0, 0, .10)'
  },
  chatAreaStyle: {
    //backgroundColor: 'orange',
    height: 'calc(100%)'
  },
  writeMessageStyle: {
    // backgroundColor:'red',
    height: 'calc(10%)',
    // height:'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'

  },
  messagesAreaStyle: {
    // backgroundColor:'purple',
    height: 'calc(80%)',
    // height:'auto',
    borderBottom: '2px solid  rgba(0, 0, 0, .10)'
  },
  currentUserStyle: {
    // backgroundColor:'green',
    height: 'calc(10%)',
    //height:'auto',
    borderBottom: '2px solid  rgba(0, 0, 0, .10)'
  },

  Input: {
    width: 'auto'
  },
  titleStyle: {
    //  fontStyle: 'oblique',
    padding: '15px 0px'
  },
})

);

export { useStyles }