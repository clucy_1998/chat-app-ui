//Only responsible for rendering a user

import React from 'react';
import { ListItemText, ListItemAvatar, Avatar, Divider } from '@material-ui/core'

function User({ contact }) {
    return (
        <React.Fragment>
            <ListItemAvatar>
                <Avatar src="" />
            </ListItemAvatar>
            <ListItemText primary={contact.username} secondary={`${contact.firstName} ${contact.lastName}`} />
            <Divider />
        </React.Fragment>


    )
}

export default User
