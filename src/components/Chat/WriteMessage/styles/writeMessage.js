import { makeStyles } from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({
  mainContainer: {
    //backgroundColor: 'pink',
    width: '100%'
  },
  input: {
    width: '80%',
    height: 'auto',
    padding: '5px 10px',
    backgroundColor: 'rgba(0, 0, 0, .07)',
    borderRadius: '15px',
    marginRight: "10px"
  },
  button: {
    textTransform: 'none',
  },

}));

export { useStyles }    