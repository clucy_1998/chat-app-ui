import React, { useEffect } from 'react';
import { connect } from 'react-redux'
//Socket
import io from "socket.io-client";

//Imports Material UIs
import { InputBase, Box } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';

//Imports for styling
import { useStyles } from './styles/writeMessage';

//Socket endpoint
const hostURL = "http://localhost:5000";
const socket = io(hostURL, {
  transports: ["websocket", "polling"],
});

function WriteMessage(props) {

  const classes = useStyles();
  const contact = {
    id: `${props.contactId}`,
    name: `${props.contactName}`,
    status: `${props.contactStatus}`
  }
  const [messageValue, setMessageValue] = React.useState('');

  //Socket

  const sendMessageHandler = () => {
    console.log("TRIMITE MESAJ")
    if (messageValue.trim() !== '') {
      socket.emit("chat message", {
        receiver: contact.id,
        content: messageValue
      });
    }


    //Delte the message from the text input
    setMessageValue('');
  }


  useEffect(() => {
    socket.on("new message", data => {
      console.log(data)
    });
  }, []);


  return (
    <Box display="flex" justifyContent="center" className={classes.mainContainer} >

      <InputBase
        className={classes.input}
        inputProps={{ 'aria-label': 'naked' }}
        value={messageValue}
        onChange={e => setMessageValue(e.target.value)}
        multiline
        rowsMax={2}
      />
      <Button
        type="submit"
        variant="contained"
        color="primary"
        className={classes.button}
        endIcon={<SendIcon>send</SendIcon>}
        onClick={sendMessageHandler}
      >
        Send
      </Button>
    </Box>


  )
}
const mapStateToProps = state => {
  return {
    contactName: state.activeContact.contactName,
    contactId: state.activeContact.contactId,
    contactStatus: state.activeContact.contactStatus
  }
}
export default connect(mapStateToProps)(WriteMessage)


