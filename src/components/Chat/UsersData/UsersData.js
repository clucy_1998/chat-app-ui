
export default [
    {
        id: 1,
        name: 'John Carter',
        status: 'online'
    },
    {
        id: 2,
        name: 'Emily Watts',
        status: 'online'
    },
    {
        id: 3,
        name: 'Emille Gills',
        status: 'offline'
    },
    {
        id: 4,
        name: 'Veronica Micle',
        status: 'online'
    },
    {
        id: 5,
        name: 'Constantin DUdu',
        status: 'online'
    },
    {
        id: 6,
        name: 'Some other fucking guy',
        status: 'offline'
    },
    {
        id: 7,
        name: 'John Carter',
        status: 'online'
    },
    {
        id: 8,
        name: 'Lisa Eldenstein',
        status: 'online'
    },
    {
        id: 9,
        name: 'George Clooney',
        status: 'offline'
    }
]