import { makeStyles } from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({
  listStyle: {
    width: "100%",
    // backgroundColor: "yellow",
    height: 'auto',
    alignSelf: 'left',
  },
  textStyle: {
    color: "green"
  },

  userCardStyle: {
    //  backgroundColor: "yellow",
    width: 200
  },
  listItemStyle: {
    marginLeft: '0px',

    // backgroundColor: "green",
    '&:hover $moreButton': {
      // display: 'block',
      visibility: 'visible'
      // moreButton: {
      //   display: 'block',
      //   backgroundColor: "yellow"
      // }

    }
  },
  search: {
    width: '100%',


  },
  searchBox: {
    width: '90%',
    borderRadius: '25px',
    backgroundColor: 'rgba(0, 0, 0, .04)',
    color: 'rgba(0, 0, 0, .40)',
    padding: '0px 5px'
  },


  searchIcon: {
    "&:hover": {
      backgroundColor: "transparent"
    }

  },
  clearButton: {
    height: '15px',
    width: '15px'
  },
  moreButton: {
    // display: 'none',
    visibility: 'hidden'

  },
  snackbar: {
    color: 'red',
  },
  menu: {

  }

}));

export { useStyles }