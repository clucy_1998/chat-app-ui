//Only responsible for rendering a list of users in Chat

import React, { useState, useRef, useEffect, useLayoutEffect } from 'react';
import { connect } from 'react-redux'
import { setActiveContact } from '../../../redux';

//Imports Material UI Components
import { Box, ListItem, List, IconButton, Button, InputBase } from '@material-ui/core/';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Snackbar from '@material-ui/core/Snackbar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import CloseIcon from '@material-ui/icons/Close';

//Imports for Components

import User from '../SingleContact/SingleContact';

//Import for styling/ Should be last always
import { useStyles } from "./styles/usersList";

//Import axios calls
import { getChatUsers, getConv, postConv, deleteConv } from './axiosCalls'

function SimpleList(props) {

  const [conv, setConv] = useState([]);
  //States
  const [chatUsers, setChatUsers] = useState([]);
  const classes = useStyles();
  const [contacts, setContacts] = useState([]);
  const listItemRef = useRef(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [loading, setLoading] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const contact = {
    id: `${props.contactId}`,
    username: `${props.contactUsername}`,
    firstName: `${props.contactfirstName}`,
    lastName: `${props.contactlastName}`,
    status: `${props.contactStatus}`
  }
  //Handles
  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  //SnackBar
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  const handleSearchChange = e => {
    if (e.target.value != null) {
      setSearchTerm(e.target.value);
      const searchResults = chatUsers.filter(user =>
        user.firstName.toLowerCase().includes((searchTerm).toLowerCase()) ||
        user.lastName.toLowerCase().includes((searchTerm).toLowerCase()) ||
        user.username.toLowerCase().includes((searchTerm).toLowerCase())
      )
        ;

      //(role => role._id.find(group => user.groups.includes(group._id)));
      setContacts(searchResults);
    }
    if (e.target.value.trim() === '') {
      //setContacts([]);
      getConv(setContacts, setConv);

    }

  };

  const clearInput = () => {
    setSearchTerm('');
    setConv(conv);
    setContacts(conv);
  }

  //UseEffect
  useEffect(() => {
    setLoading(true);
  }, []);
  useEffect(() => {

    getChatUsers(setChatUsers);
    getConv(setContacts, setConv);

    console.log('use effect get chat users')
  }, [loading])

  const firstUpdate = useRef(true);


  //Show chat friends at first render
  useLayoutEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;

      getConv(setContacts, setConv);
      console.log(conv)

      return;
    }
  }, [conv]);

  //Add Contact
  const addContactHandler = (contactId) => {
    postConv({
      _id: contactId
    });

    getConv(setContacts, setConv);
    setLoading(!loading)

  }
  //Delete Contact
  const deleteContactHandler = () => {
    handleClose();
    handleMenuClose();
    deleteConv({
      _id: contact.id
    })
    getConv(setContacts, setConv);
    setLoading(!loading);
  }

  return (
    <Box display='flex' flexDirection="column" justifyContent='center'>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClick={handleMenuClose}
      >
        <MenuItem className={classes.menu} onClick={handleClick}>Delete</MenuItem>

      </Menu>
      <Snackbar
        className={classes.snackbar}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message="Delete conversation?"
        action={
          < React.Fragment >
            <Button color="secondary" size="small" onClick={deleteContactHandler} >
              DELETE
            </Button>
            <IconButton size="small" aria-label="close" color="inherit">
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment >
        }
      />
      < Box display="flex" justifyContent="space-around" alignItems="center" className={classes.search} >

        <Box display='flex' justifyContent="space-between" alignItems="center" className={classes.searchBox}>
          <SearchIcon />
          <InputBase
            placeholder="Search…"
            className={classes.Input}
            inputProps={{ 'aria-label': 'search' }}
            value={searchTerm}
            onChange={handleSearchChange}
          />
          <IconButton size="small" className={classes.searchIcon} onClick={clearInput}>
            <ClearIcon className={classes.clearButton} />
          </IconButton>
        </Box>

      </Box >
      <div >

        <List className={classes.listStyle}>


          {contacts.map((contact) => {
            if (conv.find(c => c === contact)) {
              return (
                <ListItem ref={listItemRef} button className={classes.listItemStyle} key={contact._id} onClick={() => { props.setActiveContact(contact) }} >
                  <User contact={contact} />
                  <IconButton size="small" className={classes.moreButton} aria-controls="simple-menu" aria-haspopup="true" onClick={handleMenuClick}>
                    <MoreVertIcon />
                  </IconButton>
                </ListItem>
              );
            } else {
              return (
                <Box display="flex" alignItems="center">
                  <ListItem ref={listItemRef} button className={classes.listItemStyle} key={contact._id} onClick={() => { props.setActiveContact(contact) }} >
                    <User contact={contact} />
                  </ListItem>
                  <IconButton size="small" onClick={() => addContactHandler(contact._id)}>
                    <PersonAddIcon />
                  </IconButton>
                </Box>

              );
            }

          })}
        </List>
      </div>
    </Box >

  );
}

const mapStateToProps = state => {
  return {
    activeContact: state.activeContact.contact,
    contactUsername: state.activeContact.contactUsername,
    contactId: state.activeContact.contactId,
    contactStatus: state.activeContact.contactStatus,
    contactfirstName: state.activeContact.contactfirstName,
    contactlastName: state.activeContact.contactlastName,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setActiveContact: (activeContact) => dispatch(setActiveContact(activeContact)),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SimpleList)