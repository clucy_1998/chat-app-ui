import axios from 'axios'

//Get Chat App Users

export const getChatUsers = (setChatUsers) => {
    axios.get('http://localhost:5000/api/chat/users', { withCredentials: true })
        .then(response => {

            console.log(response.data);

            setChatUsers(response.data);

        })

}

//Get User's Conversations
//DON'T USEEEEEEEEEEEEEEEEE
export const getConv = (setContacts, setConv) => {
    //Get User's friends
    axios.get('http://localhost:5000/api/chat/conversations', { withCredentials: true })
        .then(response => {
            console.log(response.data);
            console.log("retrieving friends");
            setConv(response.data)
            setContacts(response.data);

        })
}

//Post new chat friend

export const postConv = (conv) => {

    axios.post('http://localhost:5000/api/chat/conversations', conv, { withCredentials: true })
        .then(response => {

        })
}

//Delete Conversations here

export const deleteConv = (conv) => {
    //
    axios.post('http://localhost:5000/api/chat/conversations/delete', conv, { withCredentials: true })
        .then(response => {

        })
}


