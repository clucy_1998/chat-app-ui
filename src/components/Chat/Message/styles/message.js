import { makeStyles } from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({

    messageRow: {
        marginBottom: '20px',

    },
    messageContainer: {
        padding: '9px 14px',
        marginBottom: '5px',
    },
    messageTime: {
        fontSize: '0.2rem'
    },
    you: {
        color: 'white',
        marginRight: '0px',
        display: 'flex',
        flexDirection: 'row-reverse',
        '& div': {
            backgroundColor: theme.palette.primary.main,
            textAlign: 'left'
        }
    },


    other: {

        marginBottom: '5px',
        display: 'flex',
        flexDirection: 'row',
        paddingRight: '0px',
        '& div': {
            backgroundColor: 'rgba(0, 0, 0, .04)',
            textAlign: 'left'
        }
    },

    messageText: {
        padding: '7px 10px',
        borderRadius: '20px',
        wordWrap: 'break-word',
        maxWidth: '300px'
    }
}));

export { useStyles }