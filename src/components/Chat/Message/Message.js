
//A message should have a sender and a content
import React, { useEffect, useState } from 'react'

import { connect } from 'react-redux'
import { useStyles } from './styles/message'
import classNames from 'classnames'

import theme from '../../../themes/theme'

function Message(props) {

    let contact = {
        id: `${props.contactId}`,
        username: `${props.contactUsername}`,
        firstName: `${props.contactfirstName}`,
        lastName: `${props.contactlastName}`,
        status: `${props.contactStatus}`
    }

    const [message, setMessage] = useState(props.message);
    const [userId, setUserId] = useState(props.userId);

    useEffect(() => {
        //Axios Call to fetch old messages

    }, [])


    const classes = useStyles(theme);

    if (message.sender === userId) {
        return (
            <div className={classNames(classes.you, classes.messageContainer)} >
                <div className={classes.messageText}>{message.content}
                </div>
            </div>
        )
    } else {
        return (
            <div className={classNames(classes.other, classes.messageContainer)}>
                <div className={classes.messageText}>{message.content}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const userId = ownProps.userId;
    let message = ownProps.message;
    return {
        userId: userId,
        message: message,
        contactUsername: state.activeContact.contactUsername,
        contactId: state.activeContact.contactId,
        contactStatus: state.activeContact.contactStatus,
        contactfirstName: state.activeContact.contactfirstName,
        contactlastName: state.activeContact.contactlastName,
    }
}

export default connect(mapStateToProps)(Message);
