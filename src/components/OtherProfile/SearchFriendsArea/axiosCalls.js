import axios from 'axios'

//Get App Users

export const getAppUsers = (setAppUsers) => {
    axios.get('http://localhost:5000/api/chat/users', { withCredentials: true })
        .then(response => {

            console.log(response.data);

            setAppUsers(response.data);

        })

}