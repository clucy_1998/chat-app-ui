import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles(() => ({
    container: {
        //backgroundColor: 'brown',
        padding: '10px'
    },
    searchBarContainer: {

    },
    usersListContainer: {
        width: '100%',
        border: '1px solid black'
    },
    searchIcon: {
        "&:hover": {
            backgroundColor: "transparent"
        }

    },
    clearButton: {
        height: '15px',
        width: '15px'
    },
}));

export { useStyles }