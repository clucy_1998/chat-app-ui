import React, { useState, useEffect } from 'react'
import { setActiveContact } from '../../../redux';
//Imports for Styling
import { useStyles } from './styles/searchFriendsArea'

//Imports Material UI
import { Box, InputBase, IconButton, List, ListItem } from '@material-ui/core'
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import { connect } from 'react-redux'
//Import Components
import SingleContact from '../../Chat/SingleContact/SingleContact'

//Axios Callas
import { getAppUsers } from './axiosCalls'

function SearchFriendsArea(props) {
    const classes = useStyles();
    //States
    let [list, setList] = useState();
    const [appUsers, setAppUsers] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");
    const contact = {
        id: `${props.contactId}`,
        username: `${props.contactUsername}`,
        firstName: `${props.contactfirstName}`,
        lastName: `${props.contactlastName}`,
        status: `${props.contactStatus}`
    }
    const [users, setUsers] = useState([]);
    //Handles
    const handleSearchChange = e => {
        if (e.target.value != null) {
            setSearchTerm(e.target.value);
            const searchResults = appUsers.filter(user =>
                user.firstName.toLowerCase().includes((searchTerm).toLowerCase()) ||
                user.lastName.toLowerCase().includes((searchTerm).toLowerCase()) ||
                user.username.toLowerCase().includes((searchTerm).toLowerCase())
            )
                ;

            //(role => role._id.find(group => user.groups.includes(group._id)));
            //  setContacts(searchResults);
            setAppUsers(searchResults)
        }
        if (e.target.value.trim() === '') {
            setAppUsers([]);
            //   getConv(setContacts, setConv);

        }

    };

    //Clear search
    const clearInput = () => {
        setSearchTerm('')
        setAppUsers([])
    }
    //Use Effect
    useEffect(() => {

        getAppUsers(setAppUsers);
        console.log(appUsers)
        //getConv(setContacts, setConv);

        console.log('use effect get  users')

        setList(
            appUsers.map((user) => {
                return (
                    <List>
                        <ListItem button className={classes.listItemStyle}  >
                            <SingleContact contact={user} />
                        </ListItem>
                    </List>
                )

            })
        )
    }, [searchTerm])


    return (
        <Box display="flex" alignItems="center" flexDirection="column" className={classes.container}>
            <Box display="flex" alignItems="center" className={classes.searchBarContainer}>
                <SearchIcon />
                <InputBase
                    placeholder="Search…"
                    inputProps={{ 'aria-label': 'search' }}
                    value={searchTerm}
                    onChange={handleSearchChange}
                    variant="outlined"
                />
                <IconButton size="small" onClick={clearInput} >
                    <ClearIcon className={classes.clearButton} />
                </IconButton>
            </Box>
            <div className={classes.usersListContainer}>
                {list}
            </div>

        </Box>


    )
}
const mapStateToProps = state => {
    return {
        activeContact: state.activeContact.contact,
        contactUsername: state.activeContact.contactUsername,
        contactId: state.activeContact.contactId,
        contactStatus: state.activeContact.contactStatus,
        contactfirstName: state.activeContact.contactfirstName,
        contactlastName: state.activeContact.contactlastName,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setActiveContact: (activeContact) => dispatch(setActiveContact(activeContact)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchFriendsArea)

