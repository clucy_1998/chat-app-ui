import React from 'react'

//Import components
import PostsArea from '../postsArea/PostsArea'
//Imports Material UI
import { Grid } from '@material-ui/core'

//Imports for Styling
import { useStyles } from './styles/contentArea'

function ContentArea() {
    const classes = useStyles();
    return (
        <Grid container direction="column" className={classes.layout}>
            <Grid item className={classes.postsArea}>
                <PostsArea />
            </Grid>
        </Grid>
    )
}

export default ContentArea
