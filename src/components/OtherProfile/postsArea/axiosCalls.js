import axios from 'axios'

//Get User Posts
export const getUserPosts = (id, setPosts, NotLoadingPosts) => {
    axios.get('http://localhost:5000/api/user/otherProfile/posts', {
        withCredentials: true,
        params: {
            id: id
        }
    })
        .then((response) => {
            console.log('Aicii get user posts');
            console.log(response.data)
            setPosts(response.data);
            NotLoadingPosts();
        });
}
