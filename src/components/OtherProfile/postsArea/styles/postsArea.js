import { makeStyles } from '@material-ui/core'


const useStyles = makeStyles(() => ({
    postsContainer: {
        backgroundColor: 'rgba(0, 0, 0, .07)',
        padding: '10px'
    }
}));

export { useStyles }