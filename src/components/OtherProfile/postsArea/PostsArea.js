import React, { useState, useEffect } from 'react'

import { connect } from 'react-redux'
import { NotLoadingPosts } from '../../../redux';
//Import Components
import Post from '../Post/Post'
//Axios Calls
import { getUserPosts } from './axiosCalls'
//Imports for Styling
import { useStyles } from './styles/postsArea'
//aterial UI imports
import { Box } from '@material-ui/core';




function PostsArea(props) {

    const user = {
        id: `${props.userId}`,
        username: `${props.userUsername}`,
        firstName: `${props.userfirstName}`,
        lastName: `${props.userlastName}`,
    }
    //states
    let loadingPostsState = props.loadingPosts;
    // let [postLoading, setPostLoading] = useState(false);
    let [posts, setPosts] = useState([]);


    useEffect(() => {
        getUserPosts(user.id, setPosts, props.NotLoadingPosts);
        console.log("usefect for actualii puting the posts")

    }, [loadingPostsState, props.NotLoadingPosts, user.id])
    const classes = useStyles();
    return (
        <Box display="flex" flexDirection="column-reverse" className={classes.postsContainer}>
            {
                posts.map((post, index) => {
                    return <Post key={index} post={post} />
                })
            }
        </Box>
    )
}


const mapStateToProps = state => {
    return {
        loadingPosts: state.post.loadingPosts,
        userUsername: state.otherUser.userUsername,
        userId: state.otherUser.userId,
        userfirstName: state.otherUser.userfirstName,
        userlastName: state.otherUser.userslastName,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        NotLoadingPosts: () => dispatch(NotLoadingPosts())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(PostsArea)

