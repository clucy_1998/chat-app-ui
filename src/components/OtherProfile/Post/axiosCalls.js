import axios from 'axios'
//Get User Images for Posts
export const getUserImgPosts = (userId, postId, setImgSource) => {

    axios.get('http://localhost:5000/api/user/otherProfile/imgposts', {
        params: {
            userId: userId,
            postId: postId
        }, responseType: 'arraybuffer',
        withCredentials: true,


    })
        .then((response) => {
            const base64 = btoa(
                new Uint8Array(response.data).reduce(
                    (data, byte) => data + String.fromCharCode(byte),
                    '',
                ),
            );
            console.log("BAAAAAAASE")

            setImgSource(base64);

        });
}

//Post a like
export const postALike = (userId, postId) => {
    axios.post('http://localhost:5000/api/user/otherProfile/likes',
        {
            userId: userId,
            postId: postId
        },
        {

            withCredentials: true,
        })
        .then((response) => {
            console.log(response);
        });
}

//Post a DISlike
// export const dislikeAPost = (userId, postId) => {
//     axios.post('http://localhost:5000/api/user/otherProfile/dislikes',
//         {
//             userId: userId,
//             postId: postId
//         },
//         {

//             withCredentials: true,
//         })
//         .then((response) => {
//             console.log(response);
//         });
// }