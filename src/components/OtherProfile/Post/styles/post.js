import { makeStyles } from '@material-ui/core'


const useStyles = makeStyles(() => ({
    postContainer: {
        backgroundColor: 'white',
        margin: '5px 0px',
        //  border: '1px solid rgba(0, 0, 0, .07)',
        paddingBottom: ' 5px',
        // borderTopLeftRadius: "10px",
        // borderTopRightRadius: '10px'

    },
    content: {
        maxWidth: '100%',
        background: 'white',
        padding: '0px',
        marginBottom: '10px'    // borderTopLeftRadius: "10px",
        // borderTopRightRadius: '10px'
    },
    imgContainer: {
        marginTop: '5px',

        padding: '15px',
        '& img': {
            height: '100%',
            width: '100%',
            borderRadius: '10px'
        },
    },
    postTextContainer: {
        width: '100%',
        overflowWrap: 'break-all',
        wordWrap: 'break-all',
        hyphens: 'auto',
        //  backgroundColor: 'blue',
    },
    text: {
        paddingLeft: '15px',
        height: '100%',
        // backgroundColor: 'pink',
        textAlign: 'left',
        overflowWrap: 'break-word',
        wordWrap: 'break-word',
        hyphens: 'auto'
    },
    moreButtonContainer: {
        height: '25px',
        textAlign: 'right',
        backgroundColor: '#3f51b5',
        marginBottom: '15px'
        // borderTopLeftRadius: "10px",
        // borderTopRightRadius: '10px',
    },
    moreButton: {
        paddingRight: '5px',
        width: '30px',
        color: 'white'
    },
    date: {
        paddingLeft: '10px',
        color: 'white'
    }
}));

export { useStyles }