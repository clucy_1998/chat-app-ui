import React, { useState, useEffect } from 'react'

import { connect } from 'react-redux'


//Imports for Styling
import { useStyles } from './styles/post'

//Imports Material UI
import { Box, IconButton } from '@material-ui/core'
import ThumbUpOutlinedIcon from '@material-ui/icons/ThumbUpOutlined';

//Axios Calls
import { getUserImgPosts, postALike } from './axiosCalls'

function Post(props) {
    //states
    const user = {
        id: `${props.userId}`,
        username: `${props.userUsername}`,
        firstName: `${props.userfirstName}`,
        lastName: `${props.userlastName}`,
    }
    let [imgSource, setImgSource] = useState(null);
    let [imgOfPost, setImgOfPost] = useState(null);
    //Handles
    const getImgHandle = () => {
        getUserImgPosts(user.id, props.postId, setImgSource);
    }
    const postALikeHandle = () => {
        postALike(user.id, props.postId)
    }

    //effects
    useEffect(() => {
        console.log('useeffectccccccccccccccccc')
        getImgHandle();
        // props.NotLoadingPosts();
    })
    const classes = useStyles();
    useEffect(() => {
        if (!imgSource) {
            setImgOfPost(<div></div>);
        }
        else {
            setImgOfPost(

                <img alt="post" src={`data:image/jpg;base64,${imgSource}`}></img>


            )
        }
    }, [imgSource])

    return (
        <Box className={classes.content}>

            <Box display="flex" flexDirection="column" className={classes.postTextContainer}>

                <Box display="flex" justifyContent="start" alignItems="center" className={classes.moreButtonContainer} >
                    <div className={classes.date}>
                        {props.postedAt}
                    </div>
                </Box>

                <div className={classes.text}>
                    {props.postContent}
                </div>

            </Box>

            <div className={classes.imgContainer}>
                {imgOfPost}
            </div>
            <Box display="flex">
                <IconButton onClick={postALikeHandle}>
                    <ThumbUpOutlinedIcon />
                </IconButton>
            </Box>

        </Box>
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        loadingPosts: state.post.loadingPosts,
        postId: ownProps.post._id,
        postContent: ownProps.post.content,
        userUsername: state.otherUser.userUsername,
        userId: state.otherUser.userId,
        userfirstName: state.otherUser.userfirstName,
        userlastName: state.otherUser.userslastName,
        postedAt: ownProps.post.postedAt
    }
}


export default connect(mapStateToProps)(Post)
