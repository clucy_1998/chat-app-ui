import { makeStyles } from '@material-ui/core'


const useStyles = makeStyles((theme) => ({

    userAreaContainer: {
        //  backgroundColor: 'pink',
        paddingTop: '10px'

    },
    imageContainer: {
        height: '150px',
        width: '150px',
        //  backgroundColor: "orange",
        marginBottom: '10px'
    },
    avatar: {
        height: '100%',
        width: '100%'
    }

}));

export { useStyles }