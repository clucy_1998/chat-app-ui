import axios from 'axios'


//Get User Photo
export const getUserPhoto = (id, setImgSource) => {
    axios.get('http://localhost:5000/api/user/otherUserDetails/photo', {
        responseType: 'arraybuffer', withCredentials: true,
        params: {
            id: id
        }
    })
        .then((response) => {
            console.log('Aicii get profile pic');
            const base64 = btoa(
                new Uint8Array(response.data).reduce(
                    (data, byte) => data + String.fromCharCode(byte),
                    '',
                ),
            );

            setImgSource(base64);
        });
}
