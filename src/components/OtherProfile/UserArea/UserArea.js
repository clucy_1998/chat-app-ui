import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
//Imports Material UI Components
import { Avatar, Box } from '@material-ui/core/';
//import { flexbox } from '@material-ui/system';

//Imports for Styling
import { useStyles } from './styles/userArea';
import theme from '../../../themes/theme';

import { getUserPhoto } from './axiosCalls';
function UserArea(props) {
    const user = {
        id: `${props.userId}`,
        username: `${props.userUsername}`,
        firstName: `${props.userfirstName}`,
        lastName: `${props.userlastName}`,
    }
    //STATES////////////////////////////

    let [imgSource, setImgSource] = useState(null);
    //let [user, setUser] = useState({});


    useEffect(() => {
        console.log('useeffectUserArea')

        //AXIOS
        //Get User Data
        //getUserData(setUser);

        //Get User Photo
        getUserPhoto(user.id, setImgSource);

    }, [imgSource, user.id])
    const getImgHandle = () => {
        getUserPhoto(user.id, setImgSource);
    }
    const classes = useStyles(theme);
    return (
        <Box display="flex" alignItems="center" flexDirection="column" justifyContent="space-around" className={classes.userAreaContainer}>
            <Box className={classes.imageContainer}>

                <Avatar className={classes.avatar} sizes='1' alt="You profile picture" src={`data:image/jpg;base64,${imgSource}`}></Avatar>
            </Box>
            <Box>
                {user.username}
            </Box>

        </Box>

    )
}

const mapStateToProps = state => {
    return {
        userUsername: state.otherUser.userUsername,
        userId: state.otherUser.userId,
        userfirstName: state.otherUser.userfirstName,
        userlastName: state.otherUser.userslastName,
    }
}
export default connect(mapStateToProps)(UserArea)
