import { makeStyles } from '@material-ui/core'


const useStyles = makeStyles((theme) => ({

    container: {
        width: '100%',
        marginTop: '64px',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },

    avatar: {
        height: '150px',
        width: '150px'
    },
    settingsContainer: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        width: 'auto',
        height: 'auto',

        '& div:nth-child(1)': {
            margin: '10px 5px',
            position: 'static'
        }

    },
    windowSettings: {
        display: 'flex',
        width: 'auto',
        flexDirection: 'column',
        '& div': {
            margin: '10px',
            display: 'block'
        },
    },
    form: {
        width: '100%'
    },
    info: {

    },
    imageContainer: {
        paddingBottom: '15px',
        borderBottom: '1px solid grey',
    },
    input: {
        display: 'none'
    }

}));

export { useStyles }