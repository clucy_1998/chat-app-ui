import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'

//Imports Material UI
import { Avatar, Button, Typography, Input, IconButton, InputLabel, FormHelperText } from '@material-ui/core'
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import BorderColorIcon from '@material-ui/icons/BorderColor';

//Imports for Styling
import { useStyles } from './styles/settingsLayout'
import theme from '../../themes/theme'

//import axios calls
import { uploadProfilePic } from '../../redux'
import { getUserData, getUserPhoto, postNewDetails } from './axiosCalls';


function SettingsLayout(props) {
    //STATES////////////////////////////
    let [file, setFile] = useState(null);

    let [imgSource, setImgSource] = useState(null);
    let [user, setUser] = useState({});
    const [formData, setFormData] = useState({
        usernameForm: '',
        firstNameForm: '',
        lastNameForm: '',
        emailForm: '',
        passwordForm: ''
    });

    //DEF//////////////////////////////
    let [formStyle, setFormStyle] = useState({
        display: 'none'
    });
    const { firstName, lastName, email } = user;
    const { usernameForm, firstNameForm, lastNameForm, emailForm, passwordForm } = { formData };

    //HANDLES//////////////////////////
    //Upload Handle
    const uploadHandle = () => {
        const formData = new FormData();
        formData.set("file", file);
        props.uploadProfilePic(formData);
        //Get User Photo
        getUserPhoto(setImgSource);

    }
    //File Select Handler
    const fileSelectedHandler = (event) => {
        console.log(event.target.files[0])
        setFile(event.target.files[0])
    }
    //Change form handle
    const changeHandler = e => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }
    //Submit data handler
    const submitHandle = (e) => {
        e.preventDefault();
        postNewDetails(formData);
        window.location.reload(true);

    }


    useEffect(() => {
        console.log('useeffectccccccccccccccccc')

        //AXIOS
        //Get User Data
        getUserData(setUser);

        //Get User Photo
        getUserPhoto(setImgSource);

    }, [imgSource])

    //Other
    const changeFormStyle = () => {

        setFormStyle(
            () =>
                formStyle = {
                    display: 'block'
                }
        )

    }


    const classes = useStyles(theme)
    return (
        <div className={classes.container}>
            <div className={classes.settingsContainer}>
                <div className={classes.imageContainer}>
                    <Avatar className={classes.avatar} sizes='1' alt="You profile picture" src={`data:image/jpg;base64,${imgSource}`}></Avatar>
                    <input accept="image/*" className={classes.input} id="icon-button-file" type="file" name="file" onChange={fileSelectedHandler} />
                    <label htmlFor="icon-button-file">
                        <IconButton color="primary" aria-label="upload picture" component="span" >
                            <PhotoCamera />
                        </IconButton>
                        <Button size="small" color="primary" onClick={uploadHandle}>Upload</Button >
                    </label>
                </div>

                <div className={classes.windowSettings}>
                    <div className={classes.info}>
                        <div>
                            <Typography variant="subtitle1">
                                User:{firstName} {lastName}
                            </Typography>

                        </div>
                        <div>
                            <Typography variant="subtitle1">
                                E-mail address: {email}
                            </Typography>

                        </div>
                        <div>
                            <IconButton color="primary" onClick={() => changeFormStyle()}>
                                <BorderColorIcon />
                            </IconButton>

                        </div>
                    </div>
                    <div style={formStyle} className={classes.form}>
                        <form onSubmit={submitHandle} >
                            <InputLabel htmlFor="my-input">Username:</InputLabel>
                            <Input type="text" id="username" name="usernameForm" label="username" variant="outlined" value={usernameForm} onChange={changeHandler} />

                            <InputLabel htmlFor="my-input">First name:</InputLabel>
                            <Input type="text" id="firstName" name="firstNameForm" label="firstname" variant="outlined" value={firstNameForm} onChange={changeHandler} />

                            <InputLabel htmlFor="my-input">Last name:</InputLabel>
                            <Input type="text" id="lastName" name="lastNameForm" label="lastname" variant="outlined" value={lastNameForm} onChange={changeHandler} />

                            <InputLabel htmlFor="my-input">E-mail address:</InputLabel>
                            <Input type="text" id="email" name="emailForm" label="email" variant="outlined" value={emailForm} onChange={changeHandler} />

                            <InputLabel htmlFor="my-input">Password:</InputLabel>
                            <Input type="password" id="password" name="passwordForm" label="password" variant="outlined" value={passwordForm} onChange={changeHandler} />

                            <FormHelperText id="my-helper-text">Choose a strong password</FormHelperText>
                            <Button size="small" type="submit" color='primary' variant='contained'  >Submit</Button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    )
}

const mapDispathToProps = (dispatch) => {
    return {
        uploadProfilePic: (file) => dispatch(uploadProfilePic(file))
    }
}

export default connect(null, mapDispathToProps)(SettingsLayout);
