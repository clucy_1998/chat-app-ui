import axios from 'axios'

//Get User Data

export const getUserData = (setUser) => {
    axios.get('http://localhost:5000/api/user/userDetails', { withCredentials: true })
        .then(response => {
            console.log("details");
            setUser(response.data);
        })
}

//Get User Photo
export const getUserPhoto = (setImgSource) => {
    axios.get('http://localhost:5000/api/user/userDetails/photo', { responseType: 'arraybuffer', withCredentials: true })
        .then((response) => {
            const base64 = btoa(
                new Uint8Array(response.data).reduce(
                    (data, byte) => data + String.fromCharCode(byte),
                    '',
                ),
            );

            setImgSource(base64);
        });
}

//Post New User Details
export const postNewDetails = (data) => {
    axios.post('http://localhost:5000/api/user/settings/data', data, { withCredentials: true })
        .then((response) => {
            console.log("Raspuns de la server");
            console.log(response);
        });
}
