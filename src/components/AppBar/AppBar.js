import React from 'react';
import { connect } from 'react-redux'
import { useCookies } from 'react-cookie'

//Imports Material UI Components

import { AppBar, Toolbar, IconButton, Typography, Badge }
  from '@material-ui/core';

import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import MoreIcon from '@material-ui/icons/MoreVert';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/Settings';

//Imports for Routing
import {
  Link
} from "react-router-dom";
import { Redirect } from 'react-router'
//Import for styling/ Should be last always
import { useStyles } from "./styles/appBar";




const NavBar = (props) => {
  const [cookie, setCookie, removeCookie] = useCookies('token');


  const logOutHandle = () => {
    localStorage.setItem('isLoggedIn', false)

    removeCookie(cookie);
    window.location.reload(true);

  }

  const classes = useStyles();
  let isLoggedIn = localStorage.getItem("isLoggedIn")

  if (isLoggedIn === "true") {
    return (
      <div className={classes.grow}>
        <AppBar position="fixed">


          <Toolbar>

            {/* <Link to="#" className={classes.link}> */}
            <IconButton
              color="inherit"
              onClick={logOutHandle}
            >
              <ExitToAppIcon />
            </IconButton>



            <Typography className={classes.title} variant="h6" noWrap>
              BackBook
                </Typography>

            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <Link to="/profile" className={classes.link}>
                <IconButton
                  edge="end"
                  aria-label="account of current user"
                  aria-haspopup="true"
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
              </Link>
              <Link to="/chat" className={classes.link}>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <Badge badgeContent={1} color="secondary">
                    <MailIcon />
                  </Badge>
                </IconButton>
              </Link>
              <Link to="/settings" className={classes.link}>
                <IconButton aria-label="show 17 new notifications" color="inherit">
                  <Badge color="secondary">
                    <SettingsIcon />
                  </Badge>
                </IconButton>
              </Link>
            </div>
            <div className={classes.sectionMobile}>
              <IconButton
                aria-label="show more"
                aria-haspopup="true"
                color="inherit"
              >
                <MoreIcon />
              </IconButton>
            </div>
          </Toolbar>


        </AppBar>

      </div>
    );
  } else {
    return (
      <Redirect to='/signin' />
    )
  }


}
const mapStateToProps = (state) => {
  return {
    loginRedirect: state.user.loginRedirect
  }
}

export default connect(mapStateToProps)(NavBar)