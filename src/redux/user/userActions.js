import axios from 'axios'

import { LOGGED_IN } from './userTypes'
import { LOGIN_SUCCESS } from './userTypes'
import { LOGIN_FAILURE } from './userTypes'

import 'react-toastify/dist/ReactToastify.css';

export const LoggedIn = () => {
    return {
        type: LOGGED_IN
    }
}
export const loginSuccess = () => {
    return {
        type: LOGIN_SUCCESS
    }
}

export const loginFailure = (error) => {
    return {
        type: LOGIN_FAILURE,
        payload: error
    }
}


export const login = (formData) => {
    return (dispatch) => {
        axios.post('http://localhost:5000/api/user/login', formData, { withCredentials: true })
            .then(response => {
                console.log("AICIIII")
                console.log(response)

                localStorage.setItem('isLoggedIn', true)

            })
            .catch(error => {

                localStorage.setItem('isLoggedIn', false)
                const errorMsg = error.message
                dispatch(loginFailure(errorMsg))
                console.log(error)

            })

    }
}

export const uploadProfilePic = (data) => {
    return () => {
        console.log('Aicii dataaaa');
        axios.post('http://localhost:5000/api/user/settings/photo', data, { withCredentials: true })
            .then(response => {
                console.log(response)

            })
    }
}


