import { LOGGED_IN } from './userTypes'
import { NOT_LOGGED_IN } from './userTypes'
import { LOGIN_FAILURE } from './userTypes'

const initialState = {
    loginRedirect: null
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGGED_IN: {

            return {
                loginRedirect: true
            }
        }
        case NOT_LOGGED_IN: {

            return {
                loginRedirect: false
            }
        }
        case LOGIN_FAILURE: {
            return {
                error: action.payload
            }
        }
        default: return state
    }
}

export default userReducer