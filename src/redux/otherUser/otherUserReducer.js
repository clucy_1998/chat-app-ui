import { SET_CURRENT_USER } from './otherUserTypes'


const initialState = {
    userId: '',
    userUsername: '',
    userfirstName: '',
    userName: '',
    userImg: ''

}

const otherUserReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CURRENT_USER: {
            return {
                userId: action.payload._id,
                userUsername: action.payload.username,
                userfirstName: action.payload.firstName,
                userlastName: action.payload.lastName,
                userImg: action.payload.img
            }
        }
        default: return state
    }
}

export default otherUserReducer