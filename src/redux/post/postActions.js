import { LOADING_POSTS } from './postTypes'
import { NOT_LOADING_POSTS } from './postTypes'

export const LoadingPosts = () => {
    return {
        type: LOADING_POSTS
    }
}
export const NotLoadingPosts = () => {
    return {
        type: NOT_LOADING_POSTS
    }
}