import { LOADING_POSTS } from './postTypes'
import { NOT_LOADING_POSTS } from './postTypes'


const initialState = {
    loadingPosts: false
}


const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOADING_POSTS: {
            console.log("loadingPosts dispatched")
            return {
                loadingPosts: true
            }
        }
        case NOT_LOADING_POSTS: {
            console.log("NOTloadingPosts dispatched")
            return {
                loadingPosts: false
            }
        }

        default: return state
    }
}

export default postReducer