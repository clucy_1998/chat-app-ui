import { SET_ACTIVE_CONTACT } from './contactTypes'


const initialState = {
    contactId: '',
    contactUsername: '',
    contactStatus: '',
    contactfirstName: '',
    contactlastName: '',
    contactImg: ''

}

const contactReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ACTIVE_CONTACT: {
            return {
                contactId: action.payload._id,
                contactUsername: action.payload.username,
                contactStatus: action.payload.status,
                contactfirstName: action.payload.firstName,
                contactlastName: action.payload.lastName,
                contactImg: action.payload.img
            }
        }
        default: return state
    }
}

export default contactReducer