import {SET_ACTIVE_CONTACT} from './contactTypes'


export const setActiveContact = (contact) => {
    return {
        type: SET_ACTIVE_CONTACT,
        payload: contact
    }
}

