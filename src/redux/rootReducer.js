import { combineReducers } from 'redux'


import contactReducer from './contact/contactReducer'
import userReducer from './user/userReducer'
import postReducer from './post/postReducer'
import otherUserReducer from './otherUser/otherUserReducer'

const rootReducer = combineReducers({
    activeContact: contactReducer,
    user: userReducer,
    post: postReducer,
    otherUser: otherUserReducer
})

export default rootReducer